#!/usr/bin/env python

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG
import geni.urn as URN


tourDescription = """
**Allocate an Iris030 client connected at an FE site along with a NUC to control it.**
""";

tourInstructions = """

Do all the things.

""";

#
# Globals
#
class GLOBALS(object):
    IMAGE = 'urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-STD'
    STARTUP_SCRIPT = "/local/repository/iris_start.sh"

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#

agglist = [
    ("urn:publicid:IDN+web.powderwireless.net+authority+cm","Warnock Engineering Building"),
    ("urn:publicid:IDN+ebc.powderwireless.net+authority+cm","Eccles Broadcast Center"),
    ("urn:publicid:IDN+bookstore.powderwireless.net+authority+cm","Bookstore"),
    ("urn:publicid:IDN+humanities.powderwireless.net+authority+cm","Humanities"),
    ("urn:publicid:IDN+law73.powderwireless.net+authority+cm","Law (building 73)"),
    ("urn:publicid:IDN+madsen.powderwireless.net+authority+cm","Madsen Clinic"),
    ("urn:publicid:IDN+sagepoint.powderwireless.net+authority+cm","Sage Point"),
    ("urn:publicid:IDN+moran.powderwireless.net+authority+cm","Moran Clinic"),
    ("urn:publicid:IDN+cpg.powderwireless.net+authority+cm","Parking Garage"),
    ("urn:publicid:IDN+guesthouse.powderwireless.net+authority+cm","Guesthouse")
]

pc.defineParameter("aggregate","Fixed Endpoint Site",portal.ParameterType.STRING,
                   agglist[0],agglist)

pc.defineParameter("nuc","Radio/NUC device",portal.ParameterType.STRING,
                   "nuc2", ["nuc1","nuc2"])
                   

# Bind params.
params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node n1
n1 = request.RawPC('n1')
n1.component_manager_id = params.aggregate
n1.component_id = params.nuc  # Hardwire as this is what is connected to antenna.
n1.disk_image = GLOBALS.IMAGE
n1.addService(rspec.Execute(shell="sh", command=GLOBALS.STARTUP_SCRIPT))
bs1 = n1.Blockstore("scratch","/scratch")
bs1.placement = "any"

# Allocate the Iris
ir1 = request.RawPC('ir1')
ir1.component_manager_id = params.aggregate
ir1.component_id = 'iris1'

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

#
# Print and go!
#
pc.printRequestRSpec(request)
