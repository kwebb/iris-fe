#!/bin/bash

MYWD=`dirname $0`
SCRATCH="/scratch"
REPO="https://github.com/renew-wireless/RENEWLab.git"
PYFAROS="https://github.com/skylarkwireless/pyfaros.git"

cd $SCRATCH
sudo chown ${USER}:${GROUP} .
sudo chmod 775 .
git clone $REPO || \
    { echo "Failed to clone git repository: $REPO" && exit 1; }

cd RENEWLab
sudo ./install_soapy.sh || \
    { echo "Failed to install Soapy!" && exit 1; }

sudo ./install_pylibs.sh || \
    { echo "Failed to install Python libraries!" && exit 1; }

sudo ./install_cclibs.sh || \
    { echo "Failed to install C libraries!" && exit 1; }

sudo apt-get -q -y install python3-pip
sudo pip3 install --upgrade pip

git clone --branch v1.0 --depth 1 --single-branch $PYFAROS || \
    { echo "Failed to clone git repository: $PYFAROS" && exit 1; }
cd pyfaros/
./create_package.sh
cd dist && pip3 install *.tar.gz --ignore-installed || \
    { echo "Failed to install Pyfaros!" && exit 1; }

exit $?
